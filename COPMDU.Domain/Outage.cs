﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;

namespace COPMDU.Domain
{
    public class Outage
    {
        public int Id { get; set; }
        public int? ServiceOrderTypeId { get; set; }
        public ServiceOrderType ServiceOrderType { get; set; }
        public City City { get; set; }
        public int? CityId { get; set; }
        public DateTime ServiceOrderUpdatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
        public OutageType Type { get; set; }
        public List<OutageService> AffectedServices { get; set; }
        public string Title { get; set; }
        public int SymptomId { get; set; }
        public OutageSymptom Symptom { get; set; }
        public string Description { get; set; }
        public bool Filtered { get; set; }
        public Username OpenedBy { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime PredictionDate { get; set; }
        public string Address { get; set; }
        public string AddressComplement { get; set; }
        public string AddressComplement2 { get; set; }
        public string Node { get; set; }
        public DateTime Sla =>
            SymptomId == 3 ?
                StartDate.AddHours(1.5) :
                StartDate.AddHours(3);
        public int IdMdu { get; set; }
        public int? TechnicUserId { get; set; }
        public User TechnicUser { get; set; }
        public long Contract { get; set; }
        public int Notification { get; set; }
        public bool Active { get; set; }
        public int? ClosedById { get; set; }
        public User ClosedBy { get; set; }
        public List<OutageSignal> Signals { get; set; }
        public bool? ValidSignal { get; set; }
        public DateTime? SignalDate { get; set; }
        public DateTime? SignalExpiration { get; set; }

        public DateTime? InsertDate { get; set; }

        public DateTime? TechnicianUpdatedTime { get; set; }

        public bool NotificationFromAtlas { get; set; }

        public string Cep { get; set; }

        public bool Eligible => Contract > 0 && Notification > 0 && !Filtered && SymptomId != 1 && (City?.EnabledInteration ?? false);
    }
}
