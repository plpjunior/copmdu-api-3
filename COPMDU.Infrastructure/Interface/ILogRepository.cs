﻿using COPMDU.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Infrastructure.Interface
{
    public interface ILogRepository
    {
        IEnumerable<Log> GetAll();
        Log Get(int id);
        void Add(Log entity);
        void Update(Log entity);
        void Delete(Log entity);
        void RecordException(Exception ex, int? ticket = null);
    }
}
