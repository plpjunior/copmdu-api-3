﻿using COPMDU.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace COPMDU.Infrastructure.Interface
{
    public interface IErrorNotification
    {
        Task<bool> Insert(string message, int userId, int reasonId, int ticket);

        List<ErrorReportReason> GetReasons();
    }
}
