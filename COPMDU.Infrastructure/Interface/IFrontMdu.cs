﻿using COPMDU.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace COPMDU.Infrastructure.Interface
{
    public interface IFrontMdu
    {
        Task<TicketFrontData> GetDetail(int ticket);
    }
}
