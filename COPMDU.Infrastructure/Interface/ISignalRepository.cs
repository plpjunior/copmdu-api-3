﻿using COPMDU.Domain;
using COPMDU.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COPMDU.Infrastructure.Interface
{
    public interface ISignalRepository
    {
        string GetHtml(List<OutageSignal> signals);
        Task<List<OutageSignal>> GetSignal(Outage outage);
        bool Validate(List<OutageSignal> signals);
        byte[] GetPdf(string htmlContent);
    }
}
