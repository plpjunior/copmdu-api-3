﻿using System;


namespace COPMDU.Infrastructure.InterationException
{

    [Serializable]
    public class InvalidLoginException : System.Exception
    {
        public InvalidLoginException() : base("Usuário ou senha inválido ou usuário sem permissão.") { }
    }
}
