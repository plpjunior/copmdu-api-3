﻿using COPMDU.Infrastructure.ClassAttribute;
using COPMDU.Infrastructure.InterationException;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace COPMDU.Infrastructure.Util
{
    public static class StringTransformation
    {
        private static Func<string, bool> GetValidationFunction(PagePossibleResultAttribute page)
        {
            if (page.ValidateFunction != null)
                return page.ValidateFunction;
            else if (page.MatchRegex != null)
                return (string response) => page.MatchRegex.IsMatch(response);
            else
                return (string response) => response.Contains(page.ContainString);
        }

        /// <summary>
        /// Valida a página que recebeu de resposta a uma chamada e converte para um enum baseado nas regras atribuidas pelo attribute <see cref="PagePossibleResultAttribute"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="response"></param>
        /// <returns>Valor do enum que bate com a regra configurada no atributo.</returns>
        public static T ValidateResponse<T>(string response)
        {
            var type = typeof(T);
            var fields = type.GetFields();
            foreach (var field in fields)
            {
                var attributes = field.GetCustomAttributes(typeof(PagePossibleResultAttribute), false);
                if (attributes.Length > 0)
                {
                    var attr = (PagePossibleResultAttribute)attributes[0];
                    Func<string, bool> validationFunction = GetValidationFunction(attr);
                    if (validationFunction(response))
                        return (T)field.GetValue(null);
                }
            }
            throw new NotMappedPageResponseException(response);
        }


        private static object MatchResponseToAttribute(string response, PageResultPropertyAttribute[] attributes)
        {
            foreach (var attr in attributes)
            {
                if (attr.Active)
                {
                    var match = Regex.Match(response, attr.RegexString);
                    var value = "";

                    for (int i = 1; i < match.Groups.Count; i++)
                        value += match.Groups[i].Value;

                    if (value != "")
                        return attr.Convert(value);
                }
            }
            return null;
        }

        public static List<T> MatchResponseToListAttribute<T>(string response, PageResultListPropertyAttribute[] attributes)
             where T : new()
        {
            var type = typeof(T);
            var isPrimitive = type.IsPrimitive || type == typeof(string);
            var retList = new List<T>();
            if (attributes.Length > 0)
            {
                var attr = attributes[0];

                    var matches = Regex.Matches(response, attr.RegexString);
                    foreach (Match match in matches)
                    {
                        var value = new StringBuilder();
                        for (int i = 1; i < match.Groups.Count; i++)
                            value.Append(match.Groups[i].Value);

                    if (isPrimitive)
                        retList.Add((T)attr.Convert(value.ToString()));
                    else
                        retList.Add(ConvertResponse<T>(value.ToString()));
                }
                    
                    //throw new NotImplementedException("Implementação de MatchResponseToListAttribute suporta apenas lista de tipos primitivos ou de string.");
            }
            return retList;
        }
        /// <summary>
        /// Converte uma string para um objeto baseado nos atributos <see cref="PageResultProperty"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="response"></param>
        /// <returns>Valor do enum que bate com a regra configurada no atributo.</returns>
        public static T ConvertResponse<T>(string response) where T : new()
        {
            var type = typeof(T);
            var properties = type.GetProperties();
            var returnObj = (T)Activator.CreateInstance(type);
            foreach (var property in properties)
            {
                object matched = null;

                var singleAttributes = (PageResultPropertyAttribute[])property.GetCustomAttributes(typeof(PageResultPropertyAttribute), false);
                if (singleAttributes.Length > 0)
                    matched = MatchResponseToAttribute(response, singleAttributes);


                var multiAttributes = (PageResultListPropertyAttribute[])property.GetCustomAttributes(typeof(PageResultListPropertyAttribute), false);
                if (multiAttributes.Length > 0)
                {
                    var memberType = property.PropertyType.GetGenericArguments()[0];
                    //TODO: achar um jeito menos hardcoded de referenciar o metodo
                    var method = typeof(StringTransformation).GetMethod("MatchResponseToListAttribute");
                    var generic = method.MakeGenericMethod(memberType);
                    matched = generic.Invoke(null, new object[] { response, multiAttributes });
                }

                if (matched != null)
                    property.SetValue(returnObj, matched);
            }
            return returnObj;
        }

    }
}
