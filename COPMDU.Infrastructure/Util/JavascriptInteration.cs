﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Infrastructure.Util
{
    public static class JavascriptInteration
    {

        public static double ConvertDateToJavascriptDate(DateTime dt) =>
            dt.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;

        public static DateTime ConvertJavascriptDateToDate(long value) =>
            new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(value).ToLocalTime();

        public static double CurrentDateToJavascriptDate() =>
            ConvertDateToJavascriptDate(DateTime.Now);

    }
}
